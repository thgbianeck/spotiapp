import { Component } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  newMusics: any[] = [];
  loading: boolean;
  messageError: string;

  error: boolean;

  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.error = false;

    this.spotify.getNewReleases()
      .subscribe((data: any) => {
        this.newMusics = data;
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.error = true;
        this.messageError = err.error.error.message;
        console.log(err.error.error.message);
      });
  }


}
