import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {
    console.log('Serviço spotify pronto');
  }

  getQuery(query: string) {

    const { url, token } = environment.spotifyApp;

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });

    const urlApi = `${url}${query}`;

    return this.http.get(urlApi, { headers });
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases?limit=20')
      .pipe(map((data) => data['albums'].items));
  }

  getArtists(term: string) {
    return this.getQuery(`search?q=${term}&type=artist&limit=15`)
    .pipe(map((data) => data['artists'].items));
  }

  getArtist(id: string) {
    return this.getQuery(`artists/${id}`);
    // .pipe(map((data) => data['artists'].items));
  }

  getTopTracks(id: string) {
    return this.getQuery(`artists/${id}/top-tracks?country=${environment.spotifyApp.country}`)
    .pipe(map((data) => data['tracks']));
  }
}
